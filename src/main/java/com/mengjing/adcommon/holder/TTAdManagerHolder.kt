package com.mengjing.adcommon.holder

import android.content.Context
import com.bytedance.sdk.openadsdk.TTAdConfig
import com.bytedance.sdk.openadsdk.TTAdConstant
import com.bytedance.sdk.openadsdk.TTAdManager
import com.bytedance.sdk.openadsdk.TTAdSdk

object TTAdManagerHolder {
    private var isInit: Boolean = false

    fun get(): TTAdManager {
        if (!isInit) {
            throw RuntimeException("TTAdSdK is not init, places check.")
        }

        return TTAdSdk.getAdManager()
    }

    fun init(context: Context, appId: String, isDebug: Boolean = false) {
        val ttConfig = buildConfig(appId, isDebug)

        TTAdSdk.init(context, ttConfig)

        isInit = true
    }

    private fun buildConfig(appId: String, isDebug: Boolean): TTAdConfig {
        return TTAdConfig.Builder()
            .appId(appId)
            .useTextureView(true) //使用TextureView控件播放视频,默认为SurfaceView,当有SurfaceView冲突的场景，可以使用TextureView
            .allowShowNotify(true) //是否允许sdk展示通知栏提示
            .debug(isDebug) //测试阶段打开，可以通过日志排查问题，上线时去除该调用
            .directDownloadNetworkType(TTAdConstant.NETWORK_STATE_WIFI, TTAdConstant.NETWORK_STATE_4G, TTAdConstant.NETWORK_STATE_3G) //允许直接下载的网络状态集合
            .supportMultiProcess(true) //是否支持多进程
            .needClearTaskReset() //.httpStack(new MyOkStack3())//自定义网络库，demo中给出了okhttp3版本的样例，其余请自行开发或者咨询工作人员。
            .build()
    }
}