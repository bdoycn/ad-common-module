package com.mengjing.adcommon.utils

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.widget.Toast

object TToast {
    private var mToast: Toast? = null

    fun show(context: Context, message: String, duration: Int = Toast.LENGTH_LONG) {
        val toast = getToast(context)

        if (toast != null) {
            toast.duration = duration
            toast.setText(message)
            toast.show()
        } else {
            Log.d("TToast", "toast msg: $message")
        }
    }

    @SuppressLint("ShowToast")
    private fun getToast(context: Context?): Toast? {
        if (context == null) return mToast

        mToast = Toast.makeText(context.applicationContext, "", Toast.LENGTH_LONG)
        return mToast
    }

    fun reset() {
        mToast = null
    }
}