package com.mengjing.adcommon

import android.app.Activity
import com.mengjing.adcommon.ad.*

class Ad(private val mActivity: Activity) {
    fun initSplashAd(): SplashAd {
        val splashAd = SplashAd(mActivity)
        splashAd.init()

        return splashAd
    }

    fun initBannerAd(): BannerAd {
        val bannerAd = BannerAd(mActivity)
        bannerAd.init()

        return bannerAd
    }

    fun initRewardAd(): RewardAd {
        val rewardAd = RewardAd(mActivity)
        rewardAd.init()

        return rewardAd
    }

    fun initFullVideoAd(): FullVideoAd {
        val fullVideoAd = FullVideoAd(mActivity)
        fullVideoAd.init()

        return fullVideoAd
    }

    fun initInteractionAd(): InteractionAd {
        val interactionAd = InteractionAd(mActivity)
        interactionAd.init()

        return interactionAd
    }
}