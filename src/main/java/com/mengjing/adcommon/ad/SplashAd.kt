package com.mengjing.adcommon.ad

import android.app.Activity
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.MainThread
import com.bytedance.sdk.openadsdk.*
import com.mengjing.adcommon.holder.TTAdManagerHolder
import com.mengjing.adcommon.utils.TToast

const val AD_TIME_OUT = 3000

class SplashAd(private var mActivity: Activity): BaseAd(mActivity) {
//    Activity 是否曾经停止过
    private var mIsOnceStop: Boolean = false
    private lateinit var mSplashContainer: FrameLayout
    private lateinit var mCompleteCallback: () -> Unit

    override fun initTTSDKConfig() {
        val ttAdManager = TTAdManagerHolder.get();
        mTTAdNative = ttAdManager.createAdNative(mContext)
    }

    fun showAd(codeId: String, splashContainer: FrameLayout, completeCallback: () -> Unit) {
        mCodeId = codeId
        mSplashContainer = splashContainer
        mCompleteCallback = completeCallback

        loadAd()
    }

    private fun loadAd() {
        val adSlot = AdSlot.Builder()
                .setCodeId(mCodeId)
                .setExpressViewAcceptedSize(1080f, 1920f)
                .build()

        mTTAdNative.loadSplashAd(adSlot, AdListener(), AD_TIME_OUT)
    }

    private inner class AdListener: TTAdNative.SplashAdListener {
        @MainThread
        override fun onError(p0: Int, p1: String?) {
            Log.d(TAG, "onError: load error: code: $p0, message: $p1")
            mCompleteCallback()
        }

        @MainThread
        override fun onTimeout() {
            Log.d(TAG, "onTimeout: ")
            mCompleteCallback()
        }

        @MainThread
        override fun onSplashAdLoad(ad: TTSplashAd?) {
            Log.d(TAG, "onSplashAdLoad: ")
            if (ad == null) return

            val view = ad.splashView
            if (!mActivity.isFinishing) {
                mSplashContainer.removeAllViews()
                mSplashContainer.addView(view)
            } else {
                mCompleteCallback()
            }

            ad.setSplashInteractionListener(AdInteractionListener())
            if (ad.interactionType == TTAdConstant.INTERACTION_TYPE_DOWNLOAD) {
                ad.setDownloadListener(AdDownloadListener())
            }
        }
    }

    private inner class AdInteractionListener: TTSplashAd.AdInteractionListener {
        override fun onAdClicked(p0: View?, p1: Int) {
            Log.d(TAG, "onAdClicked: ")
        }

        override fun onAdShow(p0: View?, p1: Int) {
            Log.d(TAG, "onAdShow: ")
        }

        override fun onAdSkip() {
            Log.d(TAG, "onAdSkip: ")
            mCompleteCallback()
        }

        override fun onAdTimeOver() {
            Log.d(TAG, "onAdTimeOver: ")
            mCompleteCallback()
        }
    }

    private inner class AdDownloadListener: TTAppDownloadListener {
        var hasShow = false

        override fun onIdle() {}

        override fun onDownloadActive(totalBytes: Long, currBytes: Long, fileName: String?, appName: String?) {
            if (!hasShow) {
                showToast("下载中...")
                hasShow = true
            }
        }

        override fun onDownloadPaused(totalBytes: Long, currBytes: Long, fileName: String?, appName: String?) {
            showToast("下载暂停...")
        }

        override fun onDownloadFailed(totalBytes: Long, currBytes: Long, fileName: String?, appName: String?) {
            showToast("下载失败...")
        }

        override fun onDownloadFinished(totalBytes: Long, fileName: String?, appName: String?) {
            showToast("下载完成...")
        }

        override fun onInstalled(fileName: String?, appName: String?) {
            showToast("安装完成...")
        }
    }


    private fun showToast(msg: String) {
        TToast.show(mActivity, msg)
    }

    @Suppress("SENSELESS_COMPARISON")
    fun agencyResume() {
        if (mIsOnceStop) {
            if (mCompleteCallback != null) mCompleteCallback()
        }
    }

    fun agencyStop() {
        mIsOnceStop = true
    }
}
