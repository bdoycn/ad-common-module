package com.mengjing.adcommon.ad

import android.app.Activity
import android.content.Context
import com.bytedance.sdk.openadsdk.TTAdManager
import com.bytedance.sdk.openadsdk.TTAdNative
import com.mengjing.adcommon.holder.TTAdManagerHolder

open class BaseAd(protected val mContext: Context) {
    protected var TAG: String = "APP__${javaClass.name}"
    protected lateinit var mCodeId: String
    protected lateinit var mTTAdNative: TTAdNative

    fun init() {
        initTTSDKConfig()
    }

    protected open fun initTTSDKConfig() {
        val ttAdManager = TTAdManagerHolder.get()
        ttAdManager.requestPermissionIfNecessary(mContext);
        mTTAdNative = ttAdManager.createAdNative(mContext)
    }
}