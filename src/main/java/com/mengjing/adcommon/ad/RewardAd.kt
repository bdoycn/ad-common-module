package com.mengjing.adcommon.ad

import android.app.Activity
import android.util.Log
import android.widget.Toast
import com.bytedance.sdk.openadsdk.AdSlot
import com.bytedance.sdk.openadsdk.TTAdNative
import com.bytedance.sdk.openadsdk.TTAppDownloadListener
import com.bytedance.sdk.openadsdk.TTRewardVideoAd
import com.mengjing.adcommon.utils.TToast

class RewardAd(private val mActivity: Activity): BaseAd(mActivity) {
    private var mTTAd: TTRewardVideoAd? = null
    private var mCount: Int = 0

    /**
     * @param codeId 代码位ID
     * @param count 播放数量
     */
    fun showAd(codeId: String, count: Int) {
        mCodeId = codeId
        mCount = count

        loadAd()
    }

    private fun loadAd() {
        val adSlot = AdSlot.Builder()
                .setCodeId(mCodeId)
                .build()

        mTTAdNative.loadRewardVideoAd(adSlot, AdListener());
    }

    private inner class AdListener: TTAdNative.RewardVideoAdListener {
        override fun onError(p0: Int, p1: String?) {
            Log.d(TAG, "onError: load error: code: $p0, message: $p1")
        }

        override fun onRewardVideoAdLoad(ad: TTRewardVideoAd?) {
            mTTAd = ad
            ad?.setRewardAdInteractionListener(AdInteractionListener())
        }

        override fun onRewardVideoCached() {
            renderAd()
        }
    }

    private inner class AdInteractionListener: TTRewardVideoAd.RewardAdInteractionListener {
        private var mIsShowedNextAd = false

        private fun showNextAd() {
            if (mIsShowedNextAd) return
            if (mCount <= 0) return

            showAd(mCodeId, mCount - 1)
        }


        override fun onAdShow() {
            Log.d(TAG, "onAdShow: ")
        }

        override fun onAdVideoBarClick() {
            Log.d(TAG, "onAdVideoBarClick: ")
        }

        override fun onAdClose() {
            showNextAd()
        }

        override fun onVideoComplete() {
            showNextAd()
        }

        override fun onVideoError() {
            Log.d(TAG, "onVideoError: ")
            showNextAd()
        }

        override fun onRewardVerify(p0: Boolean, p1: Int, p2: String?, p3: Int, p4: String?) {
            Log.d(TAG, "onRewardVerify: ")
        }

        override fun onSkippedVideo() {
            showNextAd()
        }
    }

    private inner class AdDownloadListener: TTAppDownloadListener {
        private var mHasShowDownloadActive: Boolean = false

        override fun onIdle() {
            mHasShowDownloadActive = false
        }

        override fun onDownloadActive(totalBytes: Long, currBytes: Long, fileName: String, appName: String) {
            Log.d("DML", "onDownloadActive==totalBytes=$totalBytes,currBytes=$currBytes,fileName=$fileName,appName=$appName")
            if (!mHasShowDownloadActive) {
                mHasShowDownloadActive = true
                TToast.show(mActivity, "下载中，点击下载区域暂停", Toast.LENGTH_LONG)
            }
        }

        override fun onDownloadPaused(totalBytes: Long, currBytes: Long, fileName: String, appName: String) {
            Log.d("DML", "onDownloadPaused===totalBytes=$totalBytes,currBytes=$currBytes,fileName=$fileName,appName=$appName")
            TToast.show(mActivity, "下载暂停，点击下载区域继续", Toast.LENGTH_LONG)
        }

        override fun onDownloadFailed(totalBytes: Long, currBytes: Long, fileName: String, appName: String) {
            Log.d("DML", "onDownloadFailed==totalBytes=$totalBytes,currBytes=$currBytes,fileName=$fileName,appName=$appName")
            TToast.show(mActivity, "下载失败，点击下载区域重新下载", Toast.LENGTH_LONG)
        }

        override fun onDownloadFinished(totalBytes: Long, fileName: String, appName: String) {
            Log.d("DML", "onDownloadFinished==totalBytes=$totalBytes,fileName=$fileName,appName=$appName")
            TToast.show(mActivity, "下载完成，点击下载区域重新下载", Toast.LENGTH_LONG)
        }

        override fun onInstalled(fileName: String, appName: String) {
            Log.d("DML", "onInstalled==,fileName=$fileName,appName=$appName")
            TToast.show(mActivity, "安装完成，点击下载区域打开", Toast.LENGTH_LONG)
        }
    }

    private fun renderAd() {
        mTTAd?.showRewardVideoAd(mActivity)
    }
}